package sami.code;

public class Personne {

private String nom;
private String prenom;
public Personne(String nom, String prenom) {
	super();
	this.nom = nom;
	this.prenom = prenom;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getPrenom() {
	return prenom;
}
public void setPrenom(String prenom) {
	this.prenom = prenom;
}
@Override
public boolean equals(Object obj) {
	if(obj==null)return false;
	if(! (obj instanceof Personne))return false;
	Personne p=(Personne) obj;
	return this.nom.equals(p.nom)&&this.prenom.contentEquals(p.prenom);
}
@Override
public String toString() {
	return nom+prenom+super.toString();
}

}
